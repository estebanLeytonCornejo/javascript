function CATEGORIA() {


    var fecha = prompt("Introduce la fecha de nacimiento (formato : 1984)");
    var fechaP = parseInt(fecha);


    while (isNaN(fechaP && fechaP === "")) {
        fechaP = prompt("Debes introducir la fecha");
    }
    var calculo = 2018 - fechaP;
    var nino = "Categoria infantil";
    var adolescente = "Categoria adolescente";
    var adulto = "Categoria adulto";
    var senior = "Categoria senior";
    document.getElementById("centro").innerHTML = '<p id="nino">' + nino + '</p>' +
            '<p id="adolescente">' + adolescente + '</p>' +
            '<p id="adulto">' + adulto + '</p>' +
            '<p id="senior">' + senior + '</p>';
    
    document.getElementById("nino").innerHTML = nino;
    document.getElementById("adolescente").innerHTML = adolescente;
    document.getElementById("adulto").innerHTML = adulto;
    document.getElementById("senior").innerHTML = senior;


    if (fechaP < 1918 || fechaP > 2018) {
        alert("Fecha fuera de rango");

    } else {
        if (calculo < 4) {
            alert("Es muy pequeño para practicar judo");
        } else if (calculo > 5 && calculo <= 12) {
            document.getElementById("nino").style.color = "orange";            
        } else if (calculo > 13 && calculo <= 17) {
            document.getElementById("adolescente").style.color = "orange";
        } else if (calculo > 18 && calculo <= 45) {
            document.getElementById("adulto").style.color = "orange";
        } else if (calculo > 46) {
            document.getElementById("senior").style.color = "orange";
        }
    }

}


