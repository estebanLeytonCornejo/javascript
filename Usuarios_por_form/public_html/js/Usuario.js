var contenedorUsuario = [];//ARRAY CONTENEDOR DE LOS USUARIOS
var nuevoUsuario = new Usuario();//ARRAY VACÍO.
//CONSTRUCTOR DE LA CLASE USUARIO.

function Usuario(numeroSocio, dni, pass, nombre, apellido, fechaNac, localidad) {
    if (numeroSocio === undefined) {
        this.numeroSocio = "no indicado";
    } else {
        this.numeroSocio = numeroSocio;
    }
    if (dni === undefined) {
        this.dni = "no indicado";
    } else {
        this.dni = dni;
    }
    if (pass === undefined) {
        this.pass = "no indicado";
    } else {
        this.pass = pass;
    }
    if (nombre === undefined) {
        this.nombre = "no indicado";
    } else {
        this.nombre = nombre;
    }
    if (apellido === undefined) {
        this.apellido = "no indicado";
    } else {
        this.apellido = apellido;
    }
    if (fechaNac === undefined) {
        this.fechaNac = "no indicado";
    } else {
        this.fechaNac = fechaNac;
    }
    if (localidad === undefined) {
        this.localidad = "no indicado";
    } else {
        this.localidad = localidad;
    }
}
//recibe los datos que se ingresan desde el formulario y los inserta en el array
function recibeDatos(numero, dni, pass, nombre, apellido, fechaNac, localidad) {
    nuevoUsuario = new Usuario(numero, dni, pass, nombre, apellido, fechaNac, localidad);
    insertaUsuarioEnArray();
    alertDatosNuevoUsuario();
}

function compararDni() {
    document.getElementById("formulario").addEventListener("submit", function () {
        var dni = document.getElementById("dni").value;
        var usuarioEncontrado = null;
        //alert(contenedorUsuario[i].dni +  "    "+dni );
        for (var i = 0; i < contenedorUsuario.length; i++) {

            if (contenedorUsuario[i].dni != dni) {
                usuarioEncontrado = contenedorUsuario[i];
            }
        }
        if (usuarioEncontrado !== null) {
            contenedorUsuario.push(nuevoUsuario);
        } else {
            alert("YA EXISTE ESE DNI");
        }
    });
}
function alertDatosNuevoUsuario() {

    alert("DATOS INGRESADOS DE USUARIO : \n"
            + "NOMBRE : " + this.nombre + "\nAPELLIDO :" + this.apellido + "\nFECHA NAC : " + this.fechaNac + "\nLOCALIDAD : " + this.localidad
            );

}
function insertaUsuarioEnArray() {
    contenedorUsuario.push(nuevoUsuario);
}

function numeroUsuarios() {
    return contenedorUsuario.length;
}

function tabla() {
    var x = 1;
    var y = 1;
    var z = 1;
    var t = 1;
    var TRResto = parseInt(t);
    var TRCabeza = parseInt(x);
    var TH = parseInt(y);
    var TD = parseInt(z);
    var cod = "<table>";
    for (var i = 0; i < TRCabeza; i++) {
        cod += "<tr></tr>";
        for (var j = 0; j < TH; j++) {
            var datos = ["NOMBRE", "APELLIDO", "DNI", "LOCALIDAD", "FECHA NACIMIENTO"];
            for (var x = 0; x < datos.length; x++) {
                cod += '<th>' + datos[x] + '</th>';
            }
        }
        for (var j = 0; j < TH; j++) {
            cod += "<tr></tr>";

            for (var x = 0; x < contenedorUsuario.length; x++) {
                cod += '<td>' + contenedorUsuario[x].nombre + '</td>';
                cod += '<td>' + contenedorUsuario[x].apellido + '</td>';
                cod += '<td>' + contenedorUsuario[x].dni + '</td>';
                cod += '<td>' + contenedorUsuario[x].localidad + '</td>';
                cod += '<td>' + contenedorUsuario[x].fechaNac + '</td>';
                cod += "<tr></tr>";
            }
        }
    }
    document.getElementById("centro").innerHTML = cod;
}
function busquedaSocio() {
    document.getElementById("formulario").addEventListener("submit", function () {
        var dni = document.getElementById("dni").value;
        var usuarioEncontrado = null;
        for (var i = 0; i < contenedorUsuario.length; i++) {
            if (contenedorUsuario[i].dni === dni) {
                usuarioEncontrado = contenedorUsuario[i];
            }
        }
        if (usuarioEncontrado !== null) {
            document.getElementById("nombre").value = usuarioEncontrado.nombre;
            document.getElementById("apellido").value = usuarioEncontrado.apellido;
        } else {
            alert("No se ha encontrado dni");
        }
    });
}

function borrarSocio() {

    document.getElementById("formulario").addEventListener("submit", function () {
        var dni = document.getElementById("dni").value;
        for (var i = 0; i < contenedorUsuario.length; i++) {
            if (contenedorUsuario[i].dni === dni) {
                document.getElementById("nombre").value = contenedorUsuario[i].nombre;
                document.getElementById("apellido").value = contenedorUsuario[i].apellido;
                var pos = i;
            }

        }
        document.getElementById("formulario").addEventListener("submit", function () {
            var pregunta = prompt("Deseas borrar a este socio ? si / no (en minuscula)");
            if (pregunta === "si") {
                contenedorUsuario.splice(pos, 1);
                alert("Socio borrado");
            }
        });
    });
}

function modificarDatos() {

    document.getElementById("formulario").addEventListener("submit", function () {

        var dni = document.getElementById("dni").value;
        var nom;
        var apellido = document.getElementById("apellido").value;
        for (var i = 0; i < contenedorUsuario.length; i++) {
            if (contenedorUsuario[i].dni === dni) {
                document.getElementById("nombre").value = contenedorUsuario[i].nombre;
                document.getElementById("apellido").value = contenedorUsuario[i].apellido;

            }
        }

    });
}



