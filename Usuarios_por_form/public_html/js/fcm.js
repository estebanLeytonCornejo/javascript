
function frecuencia() {

    var edad = (parseFloat(prompt("introduce tu edad")));
    while (isNaN(edad)){
        edad = prompt ("Debes introducir tu edad");
    }
    var sexo = prompt("Introduce tu sexo : H - M");
    while(sexo != "H" && sexo != "M"){
        sexo = prompt("Esas letras no son validas,solo H - M");
        break;
    }
    var frecuenciaM = 225 - edad;
    var frecuenciaH = 220 - edad;
    var recuperacion = "Zona de recuperación (60%-70%). ";
    var aerobica = "Zona aeróbica (70%-80%).";
    var anaerobica = "Zona anaeróbica (80%-90%).";
    var roja = "Línea roja (90%-100%).";
    if (sexo === "H") {

        document.getElementById("centro").innerHTML = '<p>' + recuperacion + ' = ' + Math.round(frecuenciaH * 0.6) + ' - ' + Math.round(frecuenciaH * 0.7) + ' pulsaciones por minuto' + '</p>' +
                '<p>' + aerobica + ' = ' + Math.round(frecuenciaH * 0.7) + ' - ' + Math.round(frecuenciaH * 0.8) + ' pulsaciones por minuto' + '</p>' +
                '<p>' + anaerobica + ' = ' + Math.round(frecuenciaH * 0.8) + ' - ' + Math.round(frecuenciaH * 0.9) + ' pulsaciones por minuto' + '</p>' +
                '<p>' + roja + ' = ' + Math.round(frecuenciaH * 0.9) + ' - ' + Math.round(frecuenciaH) + ' pulsaciones por minuto' + '</p>';
    } else if (sexo === "M") {
        document.getElementById("centro").innerHTML = '<p>' + recuperacion + ' = ' + Math.round(frecuenciaM * 0.6) + ' - ' + Math.round(frecuenciaM * 0.7) + ' pulsaciones por minuto' + '</p>' +
                '<p>' + aerobica + ' = ' + Math.round(frecuenciaM * 0.7) + ' - ' + Math.round(frecuenciaM * 0.8) + ' pulsaciones por minuto' + '</p>' +
                '<p>' + anaerobica + ' = ' + Math.round(frecuenciaM * 0.8) + ' - ' + Math.round(frecuenciaM * 0.9) + ' pulsaciones por minuto' + '</p>' +
                '<p>' + roja + ' = ' + Math.round(frecuenciaM * 0.9) + ' - ' + Math.round(frecuenciaM) + ' pulsaciones por minuto' + '</p>';
    }


}

