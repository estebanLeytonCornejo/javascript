
function IMC() {
    
    
    var peso = prompt("peso (formato kg)");
    var pesoP = parseFloat(peso);
    while(isNaN(pesoP && pesoP === "" )){
        pesoP=prompt("Debes introducir un número");
    }
    var estatura = prompt("estatura(formato en metros)");
    var estaturaP = parseFloat(estatura);
    while(isNaN(estaturaP && estaturaP ==="") ){
        estaturaP = prompt("Debes introducir un número");
    }
    
    var calculoEstatura = estaturaP * estaturaP;
    var imc = pesoP / calculoEstatura;
    var delgadezS = "16.00: Infrapeso (delgadez severa)";
    var delgadezM = "16.00: Infrapeso (delgadez moderada)";
    var delgadezA = "16.00: Infrapeso (delgadez aceptable)";
    var pesoN = "18.50 - 24.99: Peso normal";
    var sobrepeso = "25.00 - 29.99: Sobrepeso";
    var obesoI = "30.00 - 34.99: Obeso (Tipo I)";
    var obesoII = "35.00 - 40.00: Obeso (Tipo II)";
    var obesoIII = "40.00: Obeso (Tipo III)";
    document.getElementById("centro").innerHTML = '<p id="uno">' + delgadezS + '</p> ' +
            '<p id="dos">' + delgadezM + '</p> ' + '<p id="tres">' + delgadezA + '</p> ' +
            '<p id="cuatro">' + pesoN + '</p> ' + '<p id="cinco">' + sobrepeso + '</p> ' +
            '<p id="seis">' + obesoI + '</p> ' + '<p id="siete">' + obesoII + '</p> ' +
            '<p id="ocho">' + obesoIII + '</p> ';
    
    document.getElementById("uno").innerHTML = delgadezS;
    document.getElementById("dos").innerHTML = delgadezM;
    document.getElementById("tres").innerHTML = delgadezA;
    document.getElementById("cuatro").innerHTML = pesoN;
    document.getElementById("cinco").innerHTML = sobrepeso;
    document.getElementById("seis").innerHTML = obesoI;
    document.getElementById("siete").innerHTML = obesoII;
    document.getElementById("ocho").innerHTML = obesoIII;
    if (imc < 16.00) {
        document.getElementById("uno").style.color = "orange";
    } else if (imc > 16.00 && imc <= 16.99) {
        document.getElementById("dos").style.color = "orange";
    } else if (imc > 17.00 && imc <= 18.49) {
        document.getElementById("tres").style.color = "orange";
    } else if (imc > 18.50 && imc <= 24.99) {  
        document.getElementById("cuatro").style.color = "orange";
    } else if (imc > 25.00 && imc <= 29.99) {  
        document.getElementById("cinco").style.color = "orange";    
    } else if (imc > 30.00 && imc <= 34.99) {      
        document.getElementById("seis").style.color = "orange";     
    } else if (imc > 35.00 && imc <= 40.00) {        
        document.getElementById("siete").style.color = "orange";     
    } else if (imc > 40.00) {      
        document.getElementById("ocho").style.color = "orange";       
    } else {
        alert("Datos incorrectos");
    }
}

