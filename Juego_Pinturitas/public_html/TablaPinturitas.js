window.onload = cargar;

function cargar() {
    tabla();
    
    document.getElementById("rojo").addEventListener("click", colorRojo);
    document.getElementById("azul").addEventListener("click", colorAzul);
    document.getElementById("morado").addEventListener("click", colorMorado);
    document.getElementById("gris").addEventListener("click", colorGris);
    document.getElementById("naranja").addEventListener("click", colorNaranja);
    document.getElementById("blanco").addEventListener("click", colorBlanco);
    document.getElementById("borrar").addEventListener("click", borrar);
    document.getElementById("vacio").innerHTML = "SIN SELECCIÓN";
    document.getElementById("uso").innerHTML ="Clica sobre un color y después dibuja sobre la tabla !!!"

}
var colorCelda;
var celda;
var pintar = false;

function borrar(event) {
    tabla();
     document.getElementById("vacio").innerHTML = "SIN SELECCIÓN";
     document.getElementById("vacio").style = "background-color: white;";
}
function colorRojo(event) {
    document.getElementById("vacio").innerHTML = "ROJO SELECCIONADO"; 
    document.getElementById("vacio").style = "background-color: red;color:white;";
    colorCelda = "rojo";
}
function colorGris(event) {
    document.getElementById("vacio").innerHTML = "GRIS SELECCIONADO";
    document.getElementById("vacio").style = "background-color: gray;color:white;";
    colorCelda = "gris";
}
function colorMorado(event) {
    document.getElementById("vacio").innerHTML = "MORADO SELECCIONADO";
    document.getElementById("vacio").style = "background-color: purple;color:white;";
    colorCelda = "morado";
}
function colorAzul(event) {   
    document.getElementById("vacio").innerHTML = "AZUL SELECCIONADO";  
    document.getElementById("vacio").style = "background-color: blue;color:white;";
    colorCelda = "azul";
}

function colorNaranja(event) {
    document.getElementById("vacio").innerHTML = "NARANJA SELECCIONADO";
    document.getElementById("vacio").style = "background-color: orange;color:white;";
    colorCelda = "naranja";
}
function colorBlanco(event) {
    document.getElementById("vacio").innerHTML = "BLANCO SELECCIONADO";
    document.getElementById("vacio").style = "background-color: white;";
    colorCelda = "blanco";
}

//function noPinta(event) {
//    for (var i = 0; i < document.getElementById("pinturitas").getElementsByTagName("td").length; i++) {
//        document.getElementById("pinturitas").getElementsByTagName("td")[i].removeEventListener("mouseover", pinta);
//
//    }
//    var x = document.getElementById("vacio").innerHTML = "NO SE HA ENCONTRADO SELECCIÓN";
//    document.getElementById("vacio").style = "background-color: white;";
//
//}

function tabla() {
    var body = document.getElementById("pinturitas");
    var tabla = document.createElement("table");
    var tblBody = document.createElement("tbody");
    body.innerHTML="";
    for (var i = 0; i < 30; i++) {
        var hilera = document.createElement("tr");
        for (var j = 0; j < 60; j++) {
            celda = document.createElement("td");
            var textoCelda = document.createTextNode("");
            celda.appendChild(textoCelda);
            hilera.appendChild(celda);
            celda.setAttribute("id", "atributo");
            //celda.addEventListener("click", inicia);
            celda.addEventListener("click", function (event) {
                pintar = !pintar;
            });
            celda.addEventListener("mouseover", function () {
                if (pintar) {
                    celda = event.target;
                    console.log(celda);
                    celda.setAttribute("class", colorCelda);
                }
            });
            //celda.addEventListener("click", noPinta);
        }
        tblBody.appendChild(hilera);
    }
    tabla.appendChild(tblBody);
    tabla.setAttribute("class", "t1");
    body.appendChild(tabla);
}

//function colorBlanco(event) {
//    var tabla = document.getElementById("pinturitas").getElementsByTagName("td");
//
//    for (var i = 0; i < tabla.length; i++) {
//        tabla[i].style = "background-color: white;";
//    }
//    document.getElementById("vacio").innerHTML = "BORRADO";
//    document.getElementById("vacio").style = "background-color: white;";
//}












