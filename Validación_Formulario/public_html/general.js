window.onload = iniciar;
var valor;

function iniciar() {
    document.getElementById("nombre").addEventListener("blur",comprobarLongitud, true);
    document.getElementById("grupo").addEventListener("blur", comprobarLongitud, true);
    document.getElementById("publicacion").addEventListener("blur", publicacion, true);
    document.getElementById("tipo").addEventListener("change", estilosGrupos, true);
    document.getElementById("formulario").addEventListener("submit", validar, true);
    document.getElementById("localizacion").addEventListener("blur", localizacion, true);


}
function comprobarLongitud(event) {
    valor = event.target.value;
    if (campo20(valor) === true) {
        event.target.style = 'border-color:green';
    } else {
        event.target.style = 'border-color:red ';
        alert("Los datos no son correctos");
    }
}

function campo20(valor) {

    if (valor === null || valor.length > 20 || valor.length === 0) {
        return false;
    } else {
        return true;
    }
}


function publicacion(event) {
    valor = event.target.value;
    if (comprobarNumeroPubli(valor) === true) {
        event.target.style = 'border-color:green';
    } else {
        event.target.style = 'border-color:red ';
         alert("Los datos del año de publicación no son correctos");
    }
}


function comprobarNumeroPubli(valor) {
    //var elemento = document.getElementById(id).value;
    if (valor == null || isNaN(valor) || valor.length != 4) {
        return false;
    } else {
        return true;
    }
}

function compruebaEstilosGrupos(valor) {
    if (valor == null || valor.length == 0 || (valor != "punk" && valor != "rock" && valor != "pop" && valor != "indie") ){
        return false;
    } else {
        return true;
    }
}
function localizacion(event) {
    valor = event.target.value;
    if (compruebaLocalizacion(valor) === true) {
        event.target.style = 'border-color:green';
    } else {
        event.target.style = 'border-color:red  ';
        alert("Solo números");
    }
}

function compruebaLocalizacion(valor) {
    if (isNaN(valor)){
        return false;
    } else {
        return true;
    }
}
function estilosGrupos(event) {
    valor = event.target.value;  
    if (compruebaEstilosGrupos(valor) === true) {
        event.target.style = 'border-color:green';
    } else {
        event.target.style = 'background-color:red';
        alert("Debes elegir un estilo");
    }
}




//falta poner event.target.grupo.value
function validar(event) {
     event.preventDefault();
    if (campo20(event.target.nombre.value) == false || campo20(event.target.grupo.value)=== false || comprobarNumeroPubli(event.target.publicacion.value) === false ||
        compruebaEstilosGrupos(event.target.tipo.value) === false || compruebaLocalizacion(event.target.localizacion.value)==false ) {
       
        
        alert("Hay datos que no han sido rellenados correctamente");
    } else {
        alert("Datos enviados con exito ");
    }
}





