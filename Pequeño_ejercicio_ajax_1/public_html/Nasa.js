function nasa() {
    var url = "https://api.nasa.gov/planetary/apod?api_key=x52lcobGu5f5GyPhWkr3EL7lAsOwsJabTDsBtfwG";


    $.ajax({
        url: url,
        success: function (result) {
            if ("copyright" in result) {
                $("#copyright").text("Image Credits: " + result.copyright);
            } else {
                $("#copyright").text("Image Credits: " + "Public Domain");
            }

            if (result.media_type == "video") {
                $("#apod_img_id").css("display", "none");
                $("#apod_vid_id").attr("src", result.url);
            } else {
                $("#apod_vid_id").css("display", "none");
                $("#apod_img_id").attr("src", result.url);
            }
            $("#reqObject").text(url);
            $("#returnObject").text(JSON.stringify(result, null, 4));
            $("#apod_explaination").text(result.explanation);
            $("#apod_title").text(result.title);
        }
    });

}

$(document).ready(function () {
    $("#b1").click(function () {
        nasa();
    });
});

function nasa2() {
    var url = "https://api.nasa.gov/neo/rest/v1/neo/browse?api_key=x52lcobGu5f5GyPhWkr3EL7lAsOwsJabTDsBtfwG";
    $.ajax({
        url: url,
        type: "get",
        dataType: "JSON",
        contenType: "application/JSON",
        success: function (result) {
            $("#pre").text(JSON.stringify(result, null, 4));
        }
    });
}

$(document).ready(function () {
    $("#b2").click(function () {
        nasa2();
    });
});